package main

import (
	"git.fashiontech.top/study_code_cache/go-playground/array"
)

func main() {
	// rset.Test()
	// array.Test()
	// array.TestSlice()
	// array.TestSlice2()

	// a := [3]int{1, 2, 3}
	// b := []int{1, 2, 3, 4}
	// array.TestSlice3(a, b)
	// fmt.Println(a)
	// fmt.Println(b)

	// array.TestSlice4()
	// array.TestSlice5()
	// array.TestSlice6()
	array.TestStrings()
}
