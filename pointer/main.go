package main

import "fmt"

func double(x *int) {
	*x += *x
	x = nil
}

func main() {
	var a = 3
	double(&a)
	fmt.Println(a)

	p := &a
	double(p)
	fmt.Println(a, p == nil)

	t := 'a'
	fmt.Printf("%c", t)

	c := complex(1, 2)
	d := complex(1, 2)
	fmt.Println(c * d)
	fmt.Println(real(c))
	fmt.Println(imag(d))
	fmt.Println(c)
}
