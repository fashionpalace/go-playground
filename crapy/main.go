package main

import (
	"fmt"
	"log"
	"os"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	GetPase()
}

func GetPase() {
	file := "myfirst.txt"
	fout, err := os.Create(file)
	defer fout.Close()
	if err != nil {
		fmt.Println(file, err)
		return
	}
	doc, err := goquery.NewDocument("https://www.fashiontech.top")
	if err != nil {
		log.Fatal(err)
	}
	doc.Find("div").Each(func(i int, s *goquery.Selection) {
		fout.WriteString(s.Text())
		fmt.Println(s.Text())
		fout.WriteString("------")
		fout.WriteString("\n\r")
	})
}
