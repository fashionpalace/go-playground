package main

import (
	"fmt"
	"reflect"
	"sync"
)

func main() {
	// syncPools()
	sync.RWMutex.Lock()
	var a = sync.Map{}
	e, ok := a.Load("1")
	fmt.Println(e, ok)
	fmt.Println(a)
}

func syncPools() {
	var sp = sync.Pool{
		New: func() interface{} {
			return 100
		},
	}

	data := sp.Get().(int)
	fmt.Println(data)
	sp.Put(data)

	fmt.Println(reflect.ValueOf(sp))
}
