package array

import (
	"fmt"
	"reflect"
)

func TestSlice() {
	var a [2]int
	var b []int

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(cap(b))
	b = append(b, 2)
	fmt.Println(b)
	fmt.Println(cap(b))
}

func TestSlice2() {
	var a [2]int = [2]int{1, 2}
	var b []int
	var c []int = a[:]
	fmt.Println(b)
	fmt.Println(len(b))
	fmt.Println(cap(b))
	b = append(b, 299)
	fmt.Println(b)
	fmt.Println(len(b))
	fmt.Println(cap(b))
	fmt.Println(c)
	copy(b, c)
	fmt.Println(b)
	fmt.Println(len(b))
	fmt.Println(cap(b))
	var d = make([]int, 0)
	fmt.Println(d)
	fmt.Println(len(d))
	fmt.Println(cap(d))
}

func TestSlice3(a [3]int, b []int) {
	a[0] = 100
	b[0] = 100
}

func TestSlice4() {
	var a []int
	fmt.Println(a)
	var b []int = []int{}
	fmt.Println(b)

	if reflect.DeepEqual(a, b) {
		fmt.Println("a和b是一样的")
	} else {
		fmt.Println("a和b不是一样的")
	}

	if a == nil {
		fmt.Println("a等于nil")
	}

	if b == nil {
		fmt.Println("b等于nil")
	} else {
		fmt.Println("b不等于nil")
	}

}

func TestSlice5() {
	var a [5]int = [5]int{1, 2, 3, 4, 5}
	fmt.Println(a)
	var b []int = a[:2]
	fmt.Println(b)
	b[0] = 100
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")

	var c [1]int = [1]int{51}
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")

	a[2] = 80
	b[2] = 90
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(cap(a))
	fmt.Println(cap(b))

	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")

	a[2] = 70
	b[2] = 40
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(cap(a))
	fmt.Println(cap(b))

	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	fmt.Println(&a[0])
	fmt.Println(&b[0], "---")
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	b = append(b, c[0])
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(cap(a))
	fmt.Println(cap(b))
	a[2] = 80
	b[2] = 90
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(cap(a))
	fmt.Println(cap(b))
}

func TestSlice6() {
	str := "Hello world"
	a := str[6:]
	fmt.Println(a[0])
	// a[0] = "a"
	fmt.Println(a)
}
