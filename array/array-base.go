package array

import "fmt"

func change(array [3]int64) {
	array[0] = 10
	fmt.Println(array)
}

func change2(array *[3]int64) {
	(*array)[0] = 100
}

func Test() {
	var array [3]int64 = [3]int64{1, 2, 3}
	fmt.Println("值传进传进去之前", array)
	change(array)
	fmt.Println("值伟递传进去之后", array)

	var array1 [3]int64 = [3]int64{1, 2, 3}
	fmt.Println("地址传速信进去之前", array1)
	change2(&array1)
	fmt.Println("地址信递信进去之后", array1)
}
