package main

import "fmt"

func b() (i int) {
	defer func() {
		i++
		fmt.Println("defer2:", i)
	}()
	defer func() {
		i++
		fmt.Println("defer1:", i)
	}()
	return //或者直接写成return
}

func c() (i int) {
	i = 8
	fmt.Println("i:", i)
	return
}

func d() *int {
	var i int
	defer func() {
		i++
		fmt.Println("defer2:", i)
	}()
	defer func() {
		i++
		fmt.Println("defer1:", i)
	}()
	return &i
}

func main() {
	fmt.Println("return:", b())
	fmt.Println("return:", c())
	fmt.Println("return:", *(d()))
}
