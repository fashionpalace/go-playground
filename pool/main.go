package main

import (
	"fmt"
)

func main() {
	Pool, err := NewPool(20, 5)
	if err != nil {
		fmt.Println(err)
	}
	i := 0

	for i < 50 {
		err = Pool.Submit(Print_Test1, "并发测试！")
		if err != nil {
			fmt.Println(err)
		}
		i++
	}
}

func Print_Test1(a string) error {
	// w.str <- str
	fmt.Println("--------", a)
	return nil
}
