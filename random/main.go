package main

import (
	"math/rand"
	"time"
)

func main() {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	n1 := r.Intn(3)
	n2 := r.Intn(9)
	println(n1)
	println(n2)

	println(time.Now().Unix())
	println(time.Now().UnixNano())

}
