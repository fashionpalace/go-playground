package main

import (
	"encoding/json"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"log"
	"time"
)

type Payload struct {
	CurrentTemperature	float64	`json:"CurrentTemperature"`
	voice				int64   `json:"voice"`
	CurrentHumidity		float64	`json:"CurrentHumidity"`
}

type IotCmd struct {
	DeviceId	int64	`json:"deviceId"`
	Cmd				string   `json:"cmd"`
	IoIndex	int64 `json:"ioIndex"`
}


var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	//var payload Payload
	//err := json.Unmarshal(msg.Payload(), &payload)
	//if err != nil {
	//	fmt.Println("json err:", err)
	//}
	//fmt.Println("payload.CurrentHumidity", payload.CurrentHumidity)
	//fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())


	var iotCmd IotCmd
	err := json.Unmarshal(msg.Payload(), &iotCmd)
	if err != nil {
		fmt.Println("json err:", err)
	}
	fmt.Println("payload.CurrentHumidity", iotCmd)
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func main() {
	var broker = "gate.fashiontech.top"
	var port = 1883
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
	opts.SetClientID("c2")
	opts.SetUsername("emqx")
	opts.SetPassword("public")
	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	//publish(client)

	sub(client)

	client.Disconnect(250)
}

func publish(client mqtt.Client) {
	num := 1
	for i := 0; i < num; i++ {
		cmd := IotCmd{
			DeviceId: 3516443,
			Cmd: "0",
			IoIndex: 4,
		}
		b, err := json.Marshal(&cmd)
		if err != nil {
			log.Fatal(err.Error())
		}
		token := client.Publish("iotcmd", 0, false, b)
		token.Wait()
		time.Sleep(time.Second)
	}
}

func sub(client mqtt.Client) {
	//go func() {
	//	topic := "topic/soil_humidness"
	//	fmt.Printf("Subscribed to topic: %s", topic)
	//	for  {
	//		token := client.Subscribe(topic, 1, messagePubHandler)
	//		token.Wait()
	//
	//	}
	//}()
	//
	//go func() {
	//	topic := "topic/air_humidness"
	//	fmt.Printf("Subscribed to topic: %s", topic)
	//	for {
	//		token := client.Subscribe(topic, 1, messagePubHandler)
	//		token.Wait()
	//	}
	//}()

	//topic := "topic/home_farming"
	topic := "iotcmd"
	fmt.Printf("Subscribed to topic: %s", topic)
	for  {
		token := client.Subscribe(topic, 1, messagePubHandler)
		token.Wait()

	}
}